# Backup DB shell script

MySQL/MariaDb backup shell script.
Based on [this script](http://hudson.su/2011/03/03/mysql-backup-script).

Author: [Sergey Basov](https://gitlab.com/kukymbr).

## Usage

Rename file `backup-db.conf.dist` to `backup-db.conf` 
and set there database connection, target directory path and other options.

	mv ./backup-db.conf.dist ./backup-db.conf
	
Make `backup-db.sh` executable
 
    chmod u+x ./backup-db.sh
   
and run it.

	./backup-db.sh
	
See your `$TARGET_DIR` from `backup-db.conf` for result files.